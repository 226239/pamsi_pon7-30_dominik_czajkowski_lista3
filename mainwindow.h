#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();

    void on_create_stack_button_clicked();

    void on_pushButton_2_clicked();

    void on_clr_button_clicked();

    void on_pushButton_4_clicked();

    void on_create_stack_button_2_clicked();

    void on_pushButton_3_clicked();

    void on_clr_button_2_clicked();

    void on_load_stack_clicked();

    void on_loadstlbtt_clicked();

    void on_savebt_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H